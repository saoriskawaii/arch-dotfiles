#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# 以下自己添加

# tab自动完成文件名和命令
complete -cf sudo

# 用上下键历史记录自动完成
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

export HISTTIMEFORMAT='%F %T '
export HISTCONTROL=erasedups
export HISTSIZE=10000
export HISTFILESIZE=20000
export HISTIGNORE='history:pwd:ls:ls *:ll:w:top:df *:clear'      # 保存しないコマンド
export PROMPT_COMMAND='history -a; history -c; history -r' # 履歴のリアルタイム反映

export PS1="\[\033[38;5;66m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;96m\]\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;65m\]\w\[$(tput sgr0)\] (\[$(tput sgr0)\]\[\033[38;5;208m\]\$?\[$(tput sgr0)\]) \\$ \[$(tput sgr0)\]"
#export PS1="`printf "%${COLUMNS}s\n" "$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/') $(\date +'%-Y/%-m/%-d %-H:%M')"`$PS1"


#环境变量

set_path(){

    # Check if user id is 1000 or higher
    [ "$(id -u)" -ge 1000 ] || return

    for i in "$@";
    do
        # Check if the directory exists
        [ -d "$i" ] || continue

        # Check if it is not already in your $PATH.
        echo "$PATH" | grep -Eq "(^|:)$i(:|$)" && continue

        # Then append it to $PATH and export it
        #export PATH="${PATH}:$i"
        export PATH="$i:${PATH}"
    done
}

set_path ~/bin

# Shell exits even if ignoreeof set
export IGNOREEOF=100

# gnupg
export GPG_TTY=$(tty)

PAGER=/usr/bin/less
EDITOR=/usr/bin/vim
VISUAL=/usr/bin/scite
BROWSER=/usr/bin/w3m
#TMPDIR=/var/tmp
LANG=en_US.UTF-8
TZ='Asia/Shanghai'

# 用上下键历史记录自动完成
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Disable Ctrl+z in terminal
trap "" SIGTSTP

# Auto "cd" when entering just a path
shopt -s autocd

# Prevent overwrite of files
set -o noclobber

#「Ctrl+S」を無効化する
stty stop undef
stty start undef

function path(){
    old=$IFS
    IFS=:
    printf "%s\n" "$PATH"
    IFS=$old
}


# ls
alias l='ls -CF'
#alias ls='ls -F --color=auto'
alias ls='ls -h -l --color=auto --time-style=+"%F %H:%M"'
alias la='ls -A'
alias ll='ls -alF'
#alias ll='ls -l'
alias lla='ll -A'
alias llh='ll -h'
alias llha='ll -hA'
alias lh='ls -lh --color=auto'
alias l.='ls -d .* --color=auto'

# grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

#############################

alias c='clear'
alias s='sync'
alias e='exit'
alias rm='rm -I'
alias bc='bc -ql'
alias cp='rsync --archive --compress -hh --info=stats1,progress2 --modify-window=1'
#alias mv='rsync --archive --compress -hh --info=stats1,progress2 --modify-window=1 --remove-source-files'
alias wipe='shred -v -z --iterations=1 --random-source=/dev/urandom'
alias date='date +"%+Y/%-m/%d %k:%M"'
alias lsblk='lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL -e7'
alias astyle='astyle -A1 -p -s4 -xC80 -c'
#alias pcc='pcc -Wall -Wpedantic -Wextra'
#alias gcc='gcc -Wall -Wpedantic -Wextra'
alias CFLAGS='-Wall -Wpedantic -Wextra'
alias poweroff='sudo shutdown -h now'
alias reboot='sudo shutdown -r now'
#alias L='|$PAGER'
#alias N='>/dev/null 2>&1'
#alias N1='>/dev/null'
#alias N2='2>/dev/null'

# git
alias gs='git status'
alias gb='git branch'
alias gc='git checkout'
alias gct='git commit --gpg-sign=D3FE9B64982F8F42'
alias gg='git grep'
alias ga='git add'
alias gd='git diff'
alias gl='git log'
alias gcma='git checkout master'
alias gfu='git fetch upstream'
alias gfo='git fetch origin'
alias gmod='git merge origin/develop'
alias gmud='git merge upstream/develop'
alias gmom='git merge origin/master'
alias gcm='git commit -m'
alias gpl='git pull'
alias gp='git push'
alias gpo='git push origin'
alias gpom='git push origin master'
alias gs='git status'
alias gst='git stash'
alias gsl='git stash list'
alias gsu='git stash -u'
alias gsp='git stash pop'
alias lftp='lftp -u mike,123456 192.168.31.2'

# gpg
alias gpglk='gpg --list-keys --keyid-format=long'
alias gpglsk='gpg --list-secret-keys --keyid-format=long'
alias gpge='gpg --encrypt --recipient 14F27367B1323515B1F61A815BDC731777049B5F'
alias gpgd='gpg --decrypt'


# 自用
alias cal='cal -S -m --color=auto'
alias diff='diff -rauN --color=auto'
# 使用单词级别比较的diff
#alias diff='git diff --no-index --color-words'
alias ip='ip --color=auto'
alias fdisk='fdisk --color'
alias ducks='du -cks * | sort -rn | head'
alias lsserial='python3 ~/bin/lsserial.py'
alias vim='/usr/bin/vim'
alias vi='/usr/bin/vim'
alias echo='/home/saori/bin/awk -f /home/saori/bin/echo.awk'


umask 002


# direnv钩子
eval "$(direnv hook bash)"



# vim: set et sw=4 sts=4 tw=80 ft=sh:
