#!/home/saori/bin/awk -f
{
        l = 1
        w = 1
        c = 1
        wc += NF
        cc += 1 + length($0)
}

END {
        printf "%s\t%s\t%s\n", l ? NR : "", w ? wc : "", c ? cc : ""
}
